<?php namespace Domenichelly\Editorial\Models;

use Model;
use \October\Rain\Database\Traits\NestedTree;

/**
 * Model
 */
class Category extends Model
{
	use \October\Rain\Database\Traits\NestedTree;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'domenichelly_editorial_category';

	public $rules = [
	];

	/**
	 * @var array Translatable fields
	 */
	public $translatable = ['title', 'description', 'slug'];

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = ['title', 'description', 'parent_id', 'slug', 'active'];

	/**
	 * @var \Cms\Classes\Controller A reference to the CMS controller.
	 */
	private $controller;

	public $hasOne = [
		'parent' => ['Domenichelly\Editorial\Models\Category']
	];
	public $hasMany = [
		'children' => ['Domenichelly\Editorial\Models\Category'],
	];


	public static function boot()
	{
		// Call default functionality (required)
		parent::boot();

		// Check the translate plugin is installed
		if (!class_exists('RainLab\Translate\Behaviors\TranslatableModel')) {
			return;
		}

		// Extend the constructor of the model
		self::extend(
			function ($model) {
				// Implement the translatable behavior
				$model->implement[] = 'RainLab.Translate.Behaviors.TranslatableModel';
			}
		);
	}

	/**
	 * Returns the class name so I can compare
	 *
	 * @return string
	 */
	public static function getClassName()
	{
		return get_called_class();
	}

	public function onUpdate()
	{
		echo '<pre>';
		print_r(post());
		echo '</pre>';
		die();
	}
}