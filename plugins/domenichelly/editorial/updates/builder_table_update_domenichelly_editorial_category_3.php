<?php namespace Domenichelly\Editorial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDomenichellyEditorialCategory3 extends Migration
{
    public function up()
    {
        Schema::table('domenichelly_editorial_category', function($table)
        {
            $table->string('image', 255)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('domenichelly_editorial_category', function($table)
        {
            $table->integer('image')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
