<?php namespace Domenichelly\Editorial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDomenichellyEditorialCategory extends Migration
{
    public function up()
    {
        Schema::create('domenichelly_editorial_category', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('title', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('slug', 255)->nullable();
            $table->boolean('active')->default(0);
            $table->integer('sort_order')->nullable()->default(1);
            $table->integer('nest_left')->nullable();
            $table->integer('nest_right')->nullable();
            $table->integer('nest_depth')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('domenichelly_editorial_category');
    }
}
