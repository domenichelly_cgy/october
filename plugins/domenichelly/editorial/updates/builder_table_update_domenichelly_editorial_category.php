<?php namespace Domenichelly\Editorial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDomenichellyEditorialCategory extends Migration
{
    public function up()
    {
        Schema::table('domenichelly_editorial_category', function($table)
        {
            $table->string('seo_title', 255)->nullable();
            $table->text('seo_description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('domenichelly_editorial_category', function($table)
        {
            $table->dropColumn('seo_title');
            $table->dropColumn('seo_description');
        });
    }
}
