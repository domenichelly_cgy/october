<?php namespace Domenichelly\Editorial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDomenichellyEditorialCategory2 extends Migration
{
    public function up()
    {
        Schema::table('domenichelly_editorial_category', function($table)
        {
            $table->integer('image')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('domenichelly_editorial_category', function($table)
        {
            $table->dropColumn('image');
        });
    }
}
