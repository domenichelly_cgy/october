<?php namespace Domenichelly\Editorial\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Lang;
use Domenichelly\Editorial\Models\Category;

class Categories extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Domenichelly.Editorial', 'main-menu-editorial', 'side-menu-item');
    }


	/**
	 * Update the menu item position
	 */
	public function reorder_onMove()
	{
		$sourceNode = Category::find(post('sourceNode'));
		$targetNode = post('targetNode') ? Category::find(post('targetNode')) : null;

		if ($sourceNode == $targetNode) {
			return;
		}

		switch (post('position')) {
			case 'before':
				$sourceNode->moveBefore($targetNode);
				break;
			case 'after':
				$sourceNode->moveAfter($targetNode);
				break;
			case 'child':
				$sourceNode->makeChildOf($targetNode);
				break;
			default:
				$sourceNode->makeRoot();
				break;
		}
	}
}