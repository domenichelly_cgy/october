<?php return [
    'plugin' => [
        'name' => 'Editorial',
        'description' => 'This is a multilanguage plugin for editorail',
		'description_category' => 'Description'
    ],
	'form' => [
		'reorder' => 'Reorder category items'
	],
	'menu'  => [
		'reorder' => 'Reorder category items'
	],

];