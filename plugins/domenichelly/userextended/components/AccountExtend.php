<?php namespace Domenichelly\UserExtended\Components;

use Lang;
use Auth;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use RainLab\User\Models\Settings as UserSettings;
use Exception;

class AccountExtend extends \RainLab\User\Components\Account
{

	public function componentDetails()
	{
		return [
			'name'        => 'domenichelly.userextended::lang.account.account',
			'description' => 'domenichelly.userextended::lang.account.account_desc'
		];
	}

    /**
     * Update the user
     */
    public function onUpdate()
    {

        if (!$user = $this->user()) {
            return;
        }

		$user->fill(post());
		$file = new \System\Models\File;
		$file->data = Input::file('avatar');
		if (!empty($file->data)) {
			$file->is_public = true;
			$file->save();
			$user->avatar()->add($file);
		}


        $user->fill(post());
        $user->save();

        /*
         * Password has changed, reauthenticate the user
         */
        if (strlen(post('password'))) {
            Auth::login($user->reload(), true);
        }

        Flash::success(post('flash', Lang::get('rainlab.user::lang.account.success_saved')));

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }


}
