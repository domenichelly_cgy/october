<?php namespace Domenichelly\UserExtended;

use System\Classes\PluginBase;
use Event;

class Plugin extends PluginBase
{

	public function pluginDetails()
	{
		return [
			'name'        => 'domenichelly.userextended::lang.plugin.name',
			'description' => 'domenichelly.userextended::lang.plugin.description',
			'author'      => 'Alexey Bobkov, Samuel Georges',
			'icon'        => 'icon-user',
			'homepage'    => 'https://github.com/rainlab/user-plugin'
		];
	}

    public function registerComponents()
    {
		return [
			'Domenichelly\UserExtended\Components\AccountExtend'       => 'account',
		];
    }

    public function registerSettings()
    {
    }

	public function boot()
	{

		\RainLab\User\Controllers\Users::extendFormFields(function ($form, $model, $context){
			$form->addFields([
				'gender' => [
					'label'   => 'domenichelly.userextended::lang.users.menu_gender',
					'type'    => 'radio',
					'options'=>array(
						'1'=> 'domenichelly.userextended::lang.users.male',
						'2'=> 'domenichelly.userextended::lang.users.female',
					)
				]
			]);
		});


		\RainLab\User\Models\User::extend(function ($model){
			$model->addFillable([
				'gender'
			]);
		});

	}

}
