<?php namespace Indikator\Content\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Indikator\Content\Models\News as ItemPost;
use Redirect;
use BackendAuth;

class NewsPage extends ComponentBase
{
    public $post;

    public $categoryPage;

    public function componentDetails()
    {
        return [
            'name'        => 'indikator.content::lang.components.news_page_name',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'   => 'indikator.content::lang.components.slug',
                'type'    => 'string',
                'default' => '{{ :slug }}'
            ],
            'categoryPage' => [
                'title'   => 'indikator.content::lang.components.category_page',
                'type'    => 'dropdown',
                'default' => 'news/category'
            ],
            'redirectPage' => [
                'title'   => 'indikator.content::lang.components.redirect_page',
                'type'    => 'dropdown',
                'default' => '404'
            ]
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getRedirectPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
        $this->post = $this->page['post'] = $this->loadPost();
    }

    protected function loadPost()
    {
        /* Select post */
        $slug = $this->property('slug');
        $post = new ItemPost;

        $post = $post->isClassExtendedWith('RainLab.Translate.Behaviors.TranslatableModel')
            ? $post->transWhere('slug', $slug)
            : $post->where('slug', $slug);

        $post = $post->isPublished();

        /* Error 404 */
        if ($post->count() == 0) {
            return Redirect::to($this->property('redirectPage'));
        }

        $post = $post->first();

        /* Category url */
        if ($post && $post->categories->count()) {
            $post->categories->each(function($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });
        }

        /* Page details */
        if ($post->meta_title == '') {
            $post->meta_title = $post->title;
        }
        if ($post->meta_description == '') {
            $post->meta_description = $post->summary;
        }

        $this->page->title = $post->title;
        $this->page->description = $post->summary;
        $this->page->meta_title = $post->meta_title;
        $this->page->meta_keywords = $post->meta_keywords;
        $this->page->meta_description = $post->meta_description;

        /* Statistics */
        if (!BackendAuth::check()) {
            ItemPost::whereId($post->id)->update([
                'stat_view' => ++$post->stat_view,
                'stat_date' => date('Y-m-d H:i:s')
            ]);
        }

        /* Finish */
        return $post;
    }
}
