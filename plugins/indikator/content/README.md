# Content Plus plugin
Complete solution for __Blog__, __News__, __Portfolio__, __Slideshow__ and __Testimonials__ in one plugin! The plugin comes with a client-friendly tool for managing contents of your website. The Content Plus plugin contains everything you need!

## Main features of Blog, News and Portfolio
* __SEO friendly__ unique meta title, keywords and description.
* __Multilingual support__ with the RainLab Translation plugin.
* __Sitemap support__ with the RainLab Sitemap plugin.
* __Unlimited attachments__ images and files from the Media.
* __Unlimited categories__ can be attached to the items which can also be organised.
* __Unlimited tags__ can be attached to the items which can also be organised.
* __Automatic statistics__ via frontend component.
* __Preview feature__ for administrators.
* __Import and export__ many different datas.

## Main features of Slideshow
* __Different link types__ blog, news, portfolio and external link.
* __External plugin support__ RainLab Blog.
* __Support styles__ set color of texts and backgrounds.
* __Multilingual support__ with the RainLab Translation plugin.
* __Sorting and filtering__ the slideshow items.

## Main features of Testimonials
* __Many parameters__ name, position, company, webpage and profile image.
* __Multilingual support__ with the RainLab Translation plugin.
* __Sorting and filtering__ the testimonials items.

## Advanced permission manager
There are available the following options:
* __Manage post__ (view items)
* __Manage news__ (view items)
* __Manage portfolio__ (view items)
* __Manage slideshow__ (view items)
* __Manage testimonials__ (view items)
* __Manage categories__ (view items)
* __Manage tags__ (view items)
* __View statistics__ (view graphs)
* __Create items__ (global permission)
* __Delete items__ (global permission)
* __Reorder items__ (global permission)

## Front-end components
There are 9 components available at this time. They are similar work to the [RainLab Blog components](https://github.com/rainlab/blog-plugin).
* __Blog__: List items and show post.
* __News__: List items and show news.
* __Portfolio__: List items and show portfolio.
* __Testimonials__: List items.
* __Slideshow__: List items.
* __Categories__: Coming soon...
* __Tags__: List items.

## Supported plugins
* [Indikator Paste Content](http://octobercms.com/plugin/indikator-paste)
* [RainLab Blog](http://octobercms.com/plugin/rainlab-blog)
* [RainLab Translate](http://octobercms.com/plugin/rainlab-translate)
* [RainLab Sitemap](http://octobercms.com/plugin/rainlab-sitemap)

## Automatic statistics
You just add the "Blog post page" frontend component to the page, where the post appears. If you are logged in as administrator, the counter will not grow. It works any cases, when the visitors open the post details.

## Preview feature
You just add the "Blog posts list" and "Blog post page" frontend components to the current pages. If you modify a Blog, News or Portfolio, a "Preview" link appears along the left of the delete icon.

## Available languages
* en - English
* hu - Magyar

## Futures plans
* Add backend dashboard widgets.
* Add categories fontend components.
* Improve the documentation.
